# There are currently four upstream source repositories (and thus tarballs)
# for this single source package:
# hipamd, hip, rocclr and opencl-runtime
# This is often called MUT for multi-upstream tarballs, in documentation.
Source: rocm-hipamd
Section: devel
Homepage: https://github.com/ROCm/clr
Priority: optional
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rocm-team/rocm-hipamd.git
Vcs-Browser: https://salsa.debian.org/rocm-team/rocm-hipamd
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Maxime Chambonnet <maxzor@maxzor.eu>,
           Étienne Mollier <emollier@debian.org>,
           Cordell Bloor <cgmb@slerp.xyz>,
           Christian Kastner <ckk@debian.org>,
Build-Depends: debhelper-compat (= 13),
               clang-17,
               clang-tools-17 <!nocheck>,
               cmake,
               libamd-comgr-dev (>= 5.6.0~),
               libclang-17-dev,
               libclang-rt-17-dev,
               libhsa-runtime-dev (>= 5.6.0~),
               libnuma-dev,
               librocm-smi-dev (>= 5.2.0~) <!nocheck>,
               lld-17 <!nocheck>,
               llvm-17-dev,
               mesa-common-dev,
               rocminfo (>= 5.2.0~) <!nocheck>,
               rocm-device-libs-17 <!nocheck>,
               zlib1g-dev
Build-Depends-Indep: doxygen <!nodoc>,
                     graphviz <!nodoc>
Rules-Requires-Root: no

# Targets of the amdhip upstream repository
Package: hipcc
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends},
         clang-17,
         llvm-17,
         lld-17,
         clang-tools-17,
         rocm-device-libs-17,
         rocminfo,
         libamdhip64-dev,
         libclang-rt-17-dev,
         file,
# roc-obj* dependencies:
         libfile-which-perl,
         liburi-encode-perl
Description: C++ Runtime API and Kernel Language for AMD and NVIDIA GPUs
 HIP (Heterogeneous Interface for Portability) is a C++ Runtime API and Kernel
 Language that allows developers to create portable applications for AMD and
 NVIDIA GPUs from single source code.
 .
 Key features include:
 .
  * HIP is very thin and has little or no performance impact over coding
    directly in CUDA mode;
  * HIP allows coding in a single-source C++ programming language including
    features such as templates, C++11 lambdas, classes, namespaces, and more;
  * HIP allows developers to use the "best" development environment and tools
    on each target platform;
  * the HIPify tools automatically convert source from CUDA to HIP;
  * developers can specialize for the platform (CUDA or AMD) to tune for
    performance or handle tricky cases.
 .
 New projects can be developed directly in the portable HIP C++ language and
 can run on either NVIDIA or AMD platforms.  Additionally, HIP provides porting
 tools which make it easy to port existing CUDA codes to the HIP layer, with no
 loss of performance as compared to the original CUDA application.  HIP is not
 intended to be a drop-in replacement for CUDA, and developers should expect to
 do some manual coding and performance tuning work to complete the port.

Package: libamdhip64-5
Section: libs
Architecture: amd64 arm64 ppc64el
Depends: libamd-comgr2, ${misc:Depends}, ${shlibs:Depends}
Description: Heterogeneous Interface for Portability - AMD GPUs implementation
 This package is central to the ROCm stack. It is at the exchange point between
 the low-level libraries - kernel module ROCk, thunk ROCt, runtime ROCr, etc,
 and user-facing libraries - rocRAND, rocBLAS, rocFFT, rocSPARSE, etc.

Package: libamdhip64-dev
Section: libdevel
Architecture: amd64 arm64 ppc64el
Suggests: libamdhip64-doc
Depends: ${misc:Depends},
         libamdhip64-5 (= ${binary:Version}),
         libhiprtc-builtins5 (= ${binary:Version}),
         libamd-comgr-dev,
         libhsa-runtime-dev
Description: Header files for the AMD implementation of HIP
 The libamdhip64 library is central to the ROCm stack. It is at the exchange
 point between the low-level libraries - kernel module ROCk, thunk ROCt,
 runtime ROCr, etc, and user-facing libraries - rocRAND, rocBLAS, rocFFT,
 rocSPARSE, etc.
 .
 This package provides headers to the libamdhip64.

Package: libhiprtc-builtins5
Section: libs
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: HIP Run Time Compilation libraries
 HIP allows one to compile kernels at runtime with its hiprtc* APIs.  hipRTC
 APIs accept HIP source files in character string format as input parameters
 and create handles of programs by compiling the HIP source files without
 spawning separate processes.

Package: libamdhip64-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends: ${misc:Depends}
Description: HIP Runtime Documentation
 The libamdhip64 library is central to the ROCm stack. It is at the exchange
 point between the low-level libraries - kernel module ROCk, thunk ROCt,
 runtime ROCr, etc, and user-facing libraries - rocRAND, rocBLAS, rocFFT,
 rocSPARSE, etc.
 .
 This package provides documentation for the HIP runtime interfaces.

Package: rocm-opencl-icd
Section: libs
Architecture: amd64 arm64 ppc64el
Provides: opencl-icd
Depends: ${misc:Depends}, ${shlibs:Depends}
# Either will trigger LLVM double load bug
Breaks: mesa-opencl-icd, pocl-opencl-icd
Description: ROCm implementation of OpenCL API - ICD runtime
 This package contains the ROCm implementation of the OpenCL (Open
 Compute Language) library for AMD GPUs, which is intended for use
 with an ICD loader. OpenCL provides a standardized interface for
 computational analysis on graphical processing units.


# Targets of the opencl upstream repository
# OpenCL ICD loader - currently "not-installed"
# /!\ This one is tricky, it is already provided by
# ocl-icd-libopencl1 by vdanjean@debian.org, auto-installed (?)
# Package: libopencl
# Architecture: all
# Conflicts: ocl-icd-libopencl1
# Depends: ${misc:Depends}
# Description: TODO

# OpenCL implementation
# strip the 64 in package name?
# Package: libamdocl64
# Architecture: all
# Depends: ${misc:Depends}
# Description: TODO

# # Targets of the rocclr upstream repository
# Package: librocclr
# Architecture: all
# Depends: ${misc:Depends}
# Description: TODO
